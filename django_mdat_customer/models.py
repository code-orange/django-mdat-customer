from datetime import date
from decimal import Decimal
from io import BytesIO

from PIL import Image
from dirtyfields import DirtyFieldsMixin
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from parler.models import TranslatableModel, TranslatedFields

from django_mdat_customer.django_mdat_customer.func import mdat_random_id

try:
    from auditlog.registry import auditlog
except ImportError:
    pass


class MdatCustomers(DirtyFieldsMixin, models.Model):
    id = models.BigAutoField(primary_key=True)
    enabled = models.BooleanField(default=True)
    external_id = models.CharField(max_length=50, default=mdat_random_id)
    created_by = models.ForeignKey("self", models.CASCADE)
    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=200)
    org_tag = models.CharField(max_length=5, null=True, blank=False, unique=True)
    primary_email = models.EmailField(null=True, blank=True)
    billing_email = models.EmailField(null=True, blank=True)
    tech_email = models.EmailField(null=True, blank=True)
    vat_id = models.CharField(max_length=50, null=True, blank=True)
    company_logo = models.BinaryField(editable=True, null=True, blank=True)

    def __str__(self):
        tmp_name = self.name + " (" + self.external_id + ")"

        if self.created_by and self.created_by_id != self.id:
            tmp_name += " by " + self.created_by.__str__()

        return tmp_name

    def get_reseller_customers(self):
        return MdatCustomers.objects.filter(created_by=self).all()

    def get_reseller_customers_and_me(self):
        return MdatCustomers.objects.filter(Q(created_by=self) | Q(id=self.id)).all()

    def get_top_level_customer(self):
        if self.created_by_id == settings.MDAT_ROOT_CUSTOMER_ID:
            return self

        return self.created_by.get_top_level_customer()

    def get_item(self, itemcode):
        customer = self

        while True:
            try:
                item = MdatItems.objects.get(created_by=customer, external_id=itemcode)
            except MdatItems.DoesNotExist:
                if customer == customer.created_by:
                    return False

                customer = customer.created_by
            else:
                return item

    def get_company_logo(self):
        if self.id == self.created_by_id:
            return self.company_logo

        if self.company_logo is None:
            return self.created_by.get_company_logo()

        return self.company_logo

    def save(self, *args, **kwargs):
        if self.company_logo is not None:
            image = Image.open(BytesIO(self.company_logo))
            image_file = BytesIO()
            image.save(image_file, format="WEBP")

            self.company_logo = image_file.getvalue()

        if self.is_dirty():
            self.date_changed = timezone.now()

        super(MdatCustomers, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_customers"
        unique_together = [["created_by", "external_id"]]


if settings.PROJECT_NAME == "django-master-data-backend":
    auditlog.register(MdatCustomers, exclude_fields=["date_changed"])


class MdatItems(DirtyFieldsMixin, TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    enabled = models.BooleanField(default=True)
    external_id = models.CharField(max_length=50, default=mdat_random_id)
    created_by = models.ForeignKey(MdatCustomers, models.CASCADE)
    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)
    linked_to = models.ForeignKey("self", models.CASCADE)
    itscope_skip = models.BooleanField(default=False)
    itscope_id = models.BigIntegerField(null=True)
    icecat_id = models.CharField(max_length=100, null=True)
    cnet_id = models.CharField(max_length=100, null=True)
    bechlem_id = models.CharField(max_length=100, null=True)
    manufacturer_sku = models.CharField(null=True, max_length=1024)

    translations = TranslatedFields(
        name=models.CharField(max_length=250, null=True, blank=True),
        description_text=models.TextField(null=True, blank=True),
        description_html=models.TextField(null=True, blank=True),
        needs_translation=models.BooleanField(default=True),
    )

    def __str__(self):
        tmp_name = self.name + " (" + self.external_id + ")"

        if self.created_by and self.created_by_id != self.id:
            tmp_name += " by " + self.created_by.__str__()

        return tmp_name

    def get_price(self):
        prices = self.mdatitemprices_set.all().order_by("-valid_from")

        if len(prices) > 0:
            return prices.first().price
        else:
            return 0

    @property
    def price(self):
        return self.get_price()

    def update_price(self, item_price: Decimal):
        decimal_places = MdatItemPrices._meta.get_field("price").decimal_places

        item_price = Decimal(round(item_price, decimal_places))

        try:
            price = self.mdatitemprices_set.get(valid_from="1970-01-01")
        except MdatItemPrices.DoesNotExist:
            price = MdatItemPrices.objects.create(
                item=self,
                valid_from="1970-01-01",
                price=item_price,
            )
            price.save()
            return price

        if self.get_price() != item_price:
            price = MdatItemPrices.objects.update_or_create(
                item=self,
                valid_from=date.today(),
                price=item_price,
            )
            return price

        return self.mdatitemprices_set.all().order_by("-valid_from").first()

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.date_changed = timezone.now()

        super(MdatItems, self).save(*args, **kwargs)

        if self.linked_to_id == 1 and self.id != 1:
            self.refresh_from_db()
            self.linked_to_id = self.id
            self.save()

    class Meta:
        db_table = "mdat_items"
        unique_together = [["created_by", "external_id"]]
        indexes = [
            models.Index(fields=["itscope_id"]),
            models.Index(fields=["icecat_id"]),
            models.Index(fields=["cnet_id"]),
            models.Index(fields=["bechlem_id"]),
            models.Index(fields=["manufacturer_sku"]),
        ]


class MdatItemPrices(DirtyFieldsMixin, models.Model):
    id = models.BigAutoField(primary_key=True)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    valid_from = models.DateField(default=date(1970, 1, 1))
    price = models.DecimalField(max_digits=16, decimal_places=6)
    date_added = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.item.date_changed = timezone.now()

        super(MdatItemPrices, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_prices"
        unique_together = [["item", "valid_from"]]


class MdatItemCustomerSpecialPrices(DirtyFieldsMixin, models.Model):
    id = models.BigAutoField(primary_key=True)
    item = models.ForeignKey(MdatItems, models.DO_NOTHING)
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING)
    valid_from = models.DateField(default=date.today)
    valid_until = models.DateField(null=True)
    price = models.DecimalField(max_digits=16, decimal_places=6)
    date_added = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.item.date_changed = timezone.now()

        super(MdatItemCustomerSpecialPrices, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_customer_special_prices"
        unique_together = [["item", "customer", "valid_from"]]


class MdatItemBillOfMaterials(DirtyFieldsMixin, models.Model):
    item = models.OneToOneField(MdatItems, models.DO_NOTHING, primary_key=True)
    material = models.ForeignKey(MdatItems, models.DO_NOTHING, related_name="+")
    quantity = models.DecimalField(max_digits=19, decimal_places=6)
    date_added = models.DateTimeField(default=timezone.now)
    date_changed = models.DateTimeField(default=timezone.now)

    def save(self, *args, **kwargs):
        if self.is_dirty():
            self.item.date_changed = timezone.now()

        super(MdatItemBillOfMaterials, self).save(*args, **kwargs)

    class Meta:
        db_table = "mdat_item_bill_of_materials"
        unique_together = [["item", "material"]]
