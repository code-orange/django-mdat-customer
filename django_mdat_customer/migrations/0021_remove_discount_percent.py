# Generated by Django 3.1.14 on 2022-01-14 14:13

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0020_replace_price_by_dynamic_property"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="mdatitems",
            name="discount_percent",
        ),
    ]
