# Generated by Django 3.2.13 on 2022-04-21 15:45

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0036_translate_item_name_part_04"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mdatitems",
            name="cnet_id",
            field=models.CharField(max_length=100, null=True),
        ),
    ]
