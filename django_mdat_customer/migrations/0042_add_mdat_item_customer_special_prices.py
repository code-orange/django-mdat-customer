# Generated by Django 3.2.13 on 2022-05-06 10:25

import datetime
import dirtyfields.dirtyfields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        (
            "django_mdat_customer",
            "0041_use_mdat_random_id_as_default_for_item_external_id",
        ),
    ]

    operations = [
        migrations.CreateModel(
            name="MdatItemCustomerSpecialPrices",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                ("valid_from", models.DateField(default=datetime.date.today)),
                ("valid_until", models.DateField(null=True)),
                ("price", models.DecimalField(decimal_places=6, max_digits=16)),
                ("date_added", models.DateTimeField(default=datetime.datetime.now)),
                (
                    "customer",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.mdatcustomers",
                    ),
                ),
                (
                    "item",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.DO_NOTHING,
                        to="django_mdat_customer.mdatitems",
                    ),
                ),
            ],
            options={
                "db_table": "mdat_item_customer_special_prices",
                "unique_together": {("item", "customer", "valid_from")},
            },
            bases=(dirtyfields.dirtyfields.DirtyFieldsMixin, models.Model),
        ),
    ]
