# Generated by Django 3.1.14 on 2022-01-06 12:06

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0012_add_itscope_skip"),
    ]

    operations = [
        migrations.AddIndex(
            model_name="mdatitems",
            index=models.Index(
                fields=["itscope_id"], name="mdat_items_itscope_b25c1c_idx"
            ),
        ),
    ]
