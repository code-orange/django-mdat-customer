# Generated by Django 3.1.14 on 2022-01-20 16:29

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0021_remove_discount_percent"),
    ]

    operations = [
        migrations.AddField(
            model_name="mdatitemprices",
            name="date_added",
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
