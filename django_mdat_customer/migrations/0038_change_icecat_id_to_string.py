# Generated by Django 3.2.13 on 2022-04-21 15:51

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_mdat_customer", "0037_change_cnet_id_to_string"),
    ]

    operations = [
        migrations.AlterField(
            model_name="mdatitems",
            name="icecat_id",
            field=models.CharField(max_length=100, null=True),
        ),
    ]
