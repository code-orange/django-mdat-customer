from haystack import indexes

from django_mdat_customer.django_mdat_customer.models import MdatItems, MdatCustomers


class MdatItemsIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    mdat_id = indexes.IntegerField(model_attr="id", indexed=False)
    itscope_id = indexes.IntegerField(model_attr="itscope_id", indexed=False, null=True)
    external_id = indexes.CharField(model_attr="external_id", indexed=False)
    name = indexes.CharField(model_attr="name", indexed=False, null=True)

    def get_model(self):
        return MdatItems


class MdatCustomersIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    mdat_id = indexes.IntegerField(model_attr="id", indexed=False)
    org_tag = indexes.CharField(model_attr="org_tag", indexed=False, null=True)
    external_id = indexes.CharField(model_attr="external_id", indexed=False)
    name = indexes.CharField(model_attr="name", indexed=False, null=True)

    def get_model(self):
        return MdatCustomers
