from django.apps import apps
from django.contrib import admin
from django.db import models

from django_mdat_customer.django_mdat_customer.models import MdatCustomers

try:
    from django_my_custom_extensions.django_my_custom_extensions.forms import (
        BinaryFileInput,
    )
except ImportError:
    binary_file_input_ok = False
else:
    binary_file_input_ok = True

if binary_file_input_ok:

    class MdatCustomersAdmin(admin.ModelAdmin):
        formfield_overrides = {models.BinaryField: {"widget": BinaryFileInput}}

    admin.site.register(MdatCustomers, MdatCustomersAdmin)

for model in apps.get_app_config("django_mdat_customer").models.values():
    if binary_file_input_ok and model == MdatCustomers:
        continue

    admin.site.register(model)
